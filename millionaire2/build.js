const fs = require('fs');
const path = require('path');
const Mustache = require("mustache");
const template = fs.readFileSync("./base.html", "utf-8");
const view = require("./view.json");
const partials = {}

const files = fs.readdirSync("./partials")
files.forEach(file => {
    const key = path.basename(file, '.html')
    partials[key] = fs.readFileSync(`./partials/${file}`, "utf-8")
})

const html = Mustache.render(template, view, partials)

fs.writeFile("dist/index.html", html, err => 
    console.log(err ? err : 'index.html written!')
); 