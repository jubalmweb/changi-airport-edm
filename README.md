# Changi Airport EDM

This is a monorepo containing edms that are self-building. See each folder's `README.md` for individual instructions on how to build them.

## FAQ

#### What is required to run the builds?

All builds are based on NodeJS. Tested on Node > 10 but should basically work on majority versions from 4+.

## Bonus

#### All templates contain a `now.json` configuration file that you can easily deploy with [now.sh](http://now.sh) to easily share across. See the site's documentation to [install](https://zeit.co/docs/v2/getting-started/installation/) it on your machine. 

