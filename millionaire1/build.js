const fs = require('fs');
const path = require('path');
const Mustache = require("mustache");
const template = fs.readFileSync("./base.html", "utf-8");
const view = require("./view.json");
const partials = {}

const html = Mustache.render(template, view, {})

fs.writeFile("dist/index.html", html, err => 
    console.log(err ? err : 'index.html written!')
); 